﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class LeftDoorScript : MonoBehaviour {

    private Animator _animator = null;

	// Use this for initialization
	void Start () {
        _animator = GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _animator.SetBool("isOpen", true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            _animator.SetBool("isOpen", false);
        }
    }


}
